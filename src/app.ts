import {Router, RouterConfiguration} from 'aurelia-router';

export class App {
  router: Router;

  configureRouter(config: RouterConfiguration, router: Router): Promise<void> | PromiseLike<void> | void {
    config.title = 'Data analyzer';
    config.options.root = '/';
    config.map([
      { route: '', moduleId: 'resources/elements/data-input/data-input',   title: 'Import', nav: true },
      { route: 'table',  moduleId: 'resources/elements/data-table/data-table', title: 'Table', nav: true },
      { route: 'charts',  moduleId: 'resources/elements/data-charts/data-charts', title: 'Charts', nav: true }
    ]);

    this.router = router;
  }
}
