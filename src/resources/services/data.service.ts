export class DataService {
  private _fileData: File;
  private _data: string[][];
  private _selectedColumns: number[];

  constructor() {
  }

  set fileData(fileData: File) {
    this._fileData = fileData;
  }

  get fileData(): File {
    return this._fileData;
  }

  // set data
  set data(data: string[][]) {
    this._data = data;

    this.selectedColumns = [];
    // set all columns to be selected as default
    this._data[0].forEach((col, index) => {
      this.selectedColumns.push(index);
    });
  }

  // get data
  get data(): string[][] {
    return this._data;
  }

  // set selected columns
  set selectedColumns(selected: number[]) {
    this._selectedColumns = selected;
  }

  // get selected columns
  get selectedColumns(): number[] {
    return this._selectedColumns;
  }
}