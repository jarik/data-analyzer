import { inject } from 'aurelia-framework';
import { DataService } from '../../services/data.service';
import { Chart } from 'chart.js';
import DataUtils from '../../utils/data-utils';

@inject(DataService)
export class DataCharts {
  originalData: string[][];
  data: string[][];
  titles: string[];
  scatterChart: HTMLCanvasElement;

  constructor(private dataService: DataService) {
  }

  created(): void {
    // get original data
    this.originalData = this.dataService.data;

    // no data -> nothing to do
    if(!this.originalData) {
        return;
    }

    this.formatData();
  }

  formatData(): void {
    // get selected columns
    const selectedColumns = this.dataService.selectedColumns;

    // new data arr for selected columns
    this.data = this.originalData.slice();

    // filter selected columns
    this.originalData.forEach((row, index) => {
      if(selectedColumns.length) {
          this.data[index] = row.filter((value, i) => {
              if(selectedColumns.indexOf(i) > -1){
                  return value;
              }      
          });
      }
    });

    // separate titles row from actual data
    this.titles = this.data.shift();

    // convert values to numbers
    this.data.forEach((row, index) => {
      row = DataUtils.convertArrayValuesToNumbers(row);
    });
  }

  // component attached to DOM, can draw some charts
  attached(): void {
    if(!this.data) {
      return;
    }

    this.createScatterChart();
  }

  // create scatter chart
  createScatterChart(): void{

    // colors for different datasets
    const colors = ['rgba(0, 0, 255, 0.2)', 'rgba(255, 0, 0, 0.2)', 'rgba(0, 255, 255, 0.2)', 'rgba(255, 0, 255, 0.2)'];
    let dataSets = [];

    // create datasets for chart
    this.data.forEach((row, rowIndex) => {
      row.forEach((columnContent, columnIndex) => {
        // setup dataset
        dataSets[columnIndex] = dataSets[columnIndex] || { label: this.titles[columnIndex], data: [], backgroundColor: colors[columnIndex] };
        // add data for each dataset
        dataSets[columnIndex].data[rowIndex] = { x: rowIndex, y: columnContent };
      });
    });

    // Create chart with Chart.js
    new Chart(this.scatterChart.getContext('2d'), {
      type: 'scatter',
      data: {
          datasets: dataSets
      },
      options: {
        layout: {
          padding: {
              left: 10,
              right: 10
          }
        },
        scales: {
          xAxes: [{
            ticks: {
              min: 1,
              stepSize: 1
            },
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'Sample #'
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Value'
            }
          }]
        },
        tooltips: {
          mode: 'index',
          axis: 'y',
          callbacks: {
            label: (tooltipItem, data) => {
                let label = data.datasets[tooltipItem.datasetIndex].label || '';

                if (label) {
                    label += ': ';
                }
                label += tooltipItem.yLabel;
                return label;
            }
          }
        }
      }
    });
  }
}
