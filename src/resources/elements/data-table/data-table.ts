import { inject } from 'aurelia-framework';
import { DataService } from '../../services/data.service';
import DataUtils from '../../utils/data-utils';

@inject(DataService)
export class DataTable {
  originalData: string[][];
  data: string[][];
  titles: string[];
  averages: number[];
  medians: number[];

  constructor(private dataService: DataService) {
    this.averages = [];
    this.medians = [];
  }

  created(): void {
    // get original data
    this.originalData = this.dataService.data;

    // no data :(
      if(!this.originalData) {
        return;
    }

    this.formatData();
    this.setCalculatedInfo();
  }

  formatData(): void {
    // get selected columns
    const selectedColumns = this.dataService.selectedColumns;

    // new data arr for selected columns
    this.data = this.originalData.slice();

    // filter selected columns
    this.originalData.forEach((row, index) => {
      if(selectedColumns.length) {
          this.data[index] = row.filter((value, i) => {
              if(selectedColumns.indexOf(i) > -1){
                  return value;
              }      
          });
      }
    });

    // separate titles row from actual data
    this.titles = this.data.shift();

    // convert values to numbers
    this.data.forEach((row, index) => {
      row = DataUtils.convertArrayValuesToNumbers(row);
    });
  }

  // calculate some helpful info
  setCalculatedInfo(): void {
    let valuesByColumn = [];

    this.data.forEach((row, rowIndex) => {
      row.forEach((column, columnIndex) => {
        valuesByColumn[columnIndex] = valuesByColumn[columnIndex] || [];
        valuesByColumn[columnIndex][rowIndex] = column;
      });
    });

    // get median and average values
    valuesByColumn.forEach((value, index) => {
      this.medians.push(DataUtils.calculateMedian(valuesByColumn[index]));
      this.averages.push(DataUtils.calculateAverage(valuesByColumn[index]));
    });
  }
}
