import { inject, observable } from 'aurelia-framework';
import { DataService } from '../../services/data.service';
import DataUtils from '../../utils/data-utils';

@inject(DataService)
export class DataSelection {
  @observable files: FileList;
  private info: DataInfo;
  private selectedColumns: number[];

  constructor(private dataService: DataService) {
  }

  created(): void {
    if(!this.dataService.data) {
      return;
    }
    this.info = this.generateInfo(this.dataService.data);
    this.selectedColumns = this.dataService.selectedColumns;
  }

  // imported files changed
  filesChanged(): void {
    // no files selected
    if (!this.files.length) {
      return;
    }

    this.dataService.fileData = this.files[0];

    this.readData();
  }

  // read file
  readData(): void {
    const reader = new FileReader();
    reader.onload = (e) => {
      // set parsed data to dataService
      this.dataService.data = DataUtils.generateDataArray(e.target.result as string);
      // generate info
      this.info = this.generateInfo(this.dataService.data);
      // get selected columns
      this.selectedColumns = this.dataService.selectedColumns;
    };

    Array.from(this.files).forEach(file => {
      reader.readAsText(file);
    });
  }

  // generate some information for preview
  generateInfo(data: string[][]): DataInfo {
    let info = {} as DataInfo;

    const fileData = this.dataService.fileData;

    info.filename = fileData.name;
    info.filesize = fileData.size;
    info.preview = [];

    // set data (4 first rows) for preview
    for (let row = 0; row < 4; row++) {
      info.preview.push(data[row]);
    }

    info.rowCount = data.length - 1; // row count: first row is assumed to contain titles for columns
    info.columnCount = data[0].length; // column count

    return info;
  }

  // on column select -> pass selected columns to dataService
  onSelectColumns(): void {
    this.dataService.selectedColumns = this.selectedColumns;
  }
}

interface DataInfo extends Object {
  filename: string;
  filesize: number;
  rowCount: number;
  columnCount: number;
  preview: string[][]
}