export default class DataUtils {

  // generate data array from CSV data string
  static generateDataArray(dataStr: string): string[][] {
    const delimiter = this.getDelimiter(dataStr);
    let dataArr = [];
    let row = 0;
    let col = 0;

    // loop through every character in the data string and create array containing rows and columns
    for (let char = 0; char < dataStr.length; char++) {
      let current = dataStr[char];
      let next = dataStr[char + 1];
      dataArr[row] = dataArr[row] || [];
      dataArr[row][col] = dataArr[row][col] || '';

      // delimiter found -> change column
      if (current == delimiter) {
        col++;
        continue;
      }

      // row change with both '\r\n'
      if (current == '\r' && next == '\n') {
        row++;
        col = 0;
        char++;
        continue;
      }

      // row change with '\n' or '\r'
      if (current == '\n' || current == '\r') {
        row++;
        col = 0;
        continue;
      }

      // add char to current column
      dataArr[row][col] += current;
    }

    return dataArr;
  }

  // todo: could use some algorithm to identify delimiter string...
  static getDelimiter(dataStr: string): string {
    return ';';
  }

  // convert number values in array to numbers
  static convertArrayValuesToNumbers(array: any[]): any[] {
    array.forEach((content, i) => {
      if(!isNaN(content) && !isNaN(parseFloat(content))) {
        array[i] = Number(content);
      }
    });

    return array;
  }

  // calculate avg
  static calculateAverage(array: any[]): number {
    const accumulate = (acc, current) => acc + current;
    const sum = array.reduce(accumulate);
    const average = sum/array.length;
    return +average.toFixed(2);
  } 

  // calculate median
  static calculateMedian(array: any[]): number {
    const arr = array.slice().sort();
    const mid = Math.ceil(arr.length / 2);
    const median = arr.length % 2 == 0 ? (arr[mid - 1] + arr[mid]) / 2 : arr[mid - 1];
    return +median.toFixed(2);
  } 
}

